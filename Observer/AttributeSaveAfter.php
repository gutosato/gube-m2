<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer;

use Magento\Framework\Event\ObserverInterface;
use Gubee\Integration\Command\Publisher\AttributePublisher;
use Gubee\Integration\Command\Publisher\BrandPublisher;
use Gubee\Integration\Gateway\Config;

class AttributeSaveAfter implements ObserverInterface
{
    protected $attributePublisher;

    protected $brandPublisher;

    protected $config;

    public function __construct(
        Config $config,
        AttributePublisher $attributePublisher,
        BrandPublisher $brandPublisher
    ) {
        $this->attributePublisher = $attributePublisher;
        $this->brandPublisher = $brandPublisher;
        $this->config = $config;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $attribute = $observer->getAttribute();
        if ($attribute->getAttributeCode() == $this->config->getBrandAttribute()) {
            if (count($attribute->getOptions())) {
                foreach ($attribute->getOptions() as $option) {
                    $this->brandPublisher->execute($option);
                }
            }
        } else {
            $this->attributePublisher->execute($attribute);
        }
    }
}