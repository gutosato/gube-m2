<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer;

use Magento\Framework\Event\ObserverInterface;
use Gubee\Integration\Command\Publisher\CategoryPublisher;

class CategorySaveAfter implements ObserverInterface
{
    protected $publisher;

    public function __construct(
        CategoryPublisher $publisher
    ) {
        $this->publisher = $publisher;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->publisher->execute($observer->getCategory());
    }
}