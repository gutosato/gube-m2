<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer;

use Magento\Framework\Event\ObserverInterface;
use Gubee\Integration\Command\Publisher\ProductPublisher;

class ProductSaveAfter implements ObserverInterface
{
    protected $publisher;

    public function __construct(
        ProductPublisher $publisher
    ) {
        $this->publisher = $publisher;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->publisher->execute($observer->getProduct());
    }

}