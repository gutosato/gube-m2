<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Command;

use Magento\Framework\Exception\LocalizedException;

class CommandException extends LocalizedException
{

}
