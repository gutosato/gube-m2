<?php
declare(strict_types=1);
namespace Gubee\Integration\Engine\Gateway\Command;

use Gubee\Integration\Engine\Gateway\CommandInterface;

interface CommandPoolInterface
{

    /**
     * @param string $commandCode
     * @return CommandInterface
     */
    public function get(string $commandCode): CommandInterface;
}
