<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Command;

use Magento\Framework\ObjectManager\TMap;
use Gubee\Integration\Engine\Gateway\CommandInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\ObjectManager\TMapFactory;

class CommandPool implements CommandPoolInterface
{

    private $commands;

    public function __construct(
        TMapFactory $TMapFactory,
        array $commands = []
    ) {
        $this->commands = $TMapFactory->create(
            [
                'array' => $commands,
                'type' => CommandInterface::class
            ]
        );
    }

    /**
     * @inheirtDoc
     * @throws NotFoundException
     */
    public function get(string $commandCode): CommandInterface
    {
        if (!isset($this->commands[$commandCode])) {
            throw new NotFoundException(
                __('The "%1" command doesn\'t exist. Verify the command and try again.', $commandCode)
            );
        }

        return $this->commands[$commandCode];
    }
}
