<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Command;

use Gubee\Integration\Engine\Gateway\CommandInterface;
use Gubee\Integration\Engine\Gateway\Request\BuilderInterface;
use Gubee\Integration\Engine\Gateway\Http\TransferFactoryInterface;
use Gubee\Integration\Engine\Gateway\Http\ClientInterface;
use Gubee\Integration\Engine\Gateway\Validator\ResultInterface;
use Gubee\Integration\Engine\Gateway\Validator\ValidatorInterface;
use Gubee\Integration\Engine\Gateway\Response\HandlerInterface;
use Gubee\Integration\Engine\Gateway\ErrorMapper\ErrorMessageMapperInterface;
use Psr\Log\LoggerInterface;

class GatewayCommand implements CommandInterface
{

    protected $requestBuilder;

    protected $transferFactory;

    protected $client;

    protected $handler;

    protected $validator;

    protected $errorMessageMapper;

    protected $logger;

    /**
     * @param BuilderInterface $requestBuilder
     * @param TransferFactoryInterface $transferFactory
     * @param ClientInterface $client
     * @param LoggerInterface $logger
     * @param HandlerInterface|null $handler
     * @param ValidatorInterface|null $validator
     */
    public function __construct(
        BuilderInterface $requestBuilder,
        TransferFactoryInterface $transferFactory,
        ClientInterface $client,
        LoggerInterface $logger,
        HandlerInterface $handler = null,
        ValidatorInterface $validator = null
    ) {
        $this->requestBuilder = $requestBuilder;
        $this->transferFactory = $transferFactory;
        $this->client = $client;
        $this->handler = $handler;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     * @throws CommandException
     */
    public function execute(array $commandSubject)
    {
        $transferO = $this->transferFactory->create(
            $this->requestBuilder->build($commandSubject)
        );

        $response = $this->client->placeRequest($transferO);

        if ($this->validator !== null) {
            $result = $this->validator->validate(
                array_merge($commandSubject, ['response' => $response])
            );

            if (!$result->isValid()) {
                $this->processErrors($result);
            }
        }

        if ($this->handler) {
            $this->handler->handle(
                $commandSubject,
                $response
            );
        }
    }

    /**
     * @param ResultInterface $result
     * @throws CommandException
     */
    private function processErrors(ResultInterface $result)
    {
        $messages = [];
        $errorsSource = array_merge($result->getErrorCodes(), $result->getFailsDescription());
        foreach ($errorsSource as $errorCodeOrMessage) {
            $errorCodeOrMessage = (string)$errorCodeOrMessage;

            // error messages mapper can be not configured if payment method doesn't have custom error messages.
            if ($this->errorMessageMapper !== null) {
                $mapped = (string)$this->errorMessageMapper->getMessage($errorCodeOrMessage);
                if (!empty($mapped)) {
                    $messages[] = $mapped;
                    $errorCodeOrMessage = $mapped;
                }
            }
            $this->logger->critical('Payment Error: ' . $errorCodeOrMessage);


            throw new CommandException(
                !empty($messages)
                    ? __(implode(PHP_EOL, $messages))
                    : __('Transaction has been declined. Please try again later.')
            );
        }
    }
}
