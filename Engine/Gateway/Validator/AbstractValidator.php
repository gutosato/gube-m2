<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Validator;

use Gubee\Integration\Engine\Gateway\Validator\ValidatorInterface;
use Gubee\Integration\Engine\Gateway\Validator\ResultInterface;
use Gubee\Integration\Engine\Gateway\Validator\ResultInterfaceFactory;

abstract class AbstractValidator implements ValidatorInterface
{

    protected $resultFactory;

    /**
     * @param ResultInterfaceFactory $resultFactory
     */
    public function __construct(
        ResultInterfaceFactory $resultFactory
    ) {
        $this->resultFactory = $resultFactory;
    }

    public function createResult(bool $isValid, array $fails = [], array $errorCodes = []): ResultInterface
    {
        return $this->resultFactory->create(
            [
                'isValid' => $isValid,
                'failsDescription' => $fails,
                'errorCodes' => $errorCodes
            ]
        );
    }
}
