<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Validator;

use Magento\Framework\ObjectManager\TMap;
use Magento\Framework\ObjectManager\TMapFactory;
use Gubee\Integration\Engine\Gateway\Validator\ResultInterfaceFactory;

class ValidatorComposite extends AbstractValidator
{

    /**
     * @var ValidatorInterface | TMap
     */
    private $validators;

    /**
     * @var array
     */
    private $chainBreakingValidators;

    /**
     * @param ResultInterfaceFactory $resultFactory
     * @param TMapFactory $TMapFactory
     * @param array $validators
     * @param array $chainBreakingValidators
     */
    public function __construct(
        ResultInterfaceFactory $resultFactory,
        TMapFactory $TMapFactory,
        array $validators = [],
        array $chainBreakingValidators = []
    ) {
        $this->validators = $TMapFactory->create(
            [
                'array' => $validators,
                'type' => ValidatorInterface::class
            ]
        );
        $this->chainBreakingValidators = $chainBreakingValidators;
        parent::__construct($resultFactory);
    }

    /**
     * @inheritDoc
     */
    public function validate($validationSubject): resultInterface
    {
        $isValid = true;
        $failsDescriptionAggregate = [];
        $errorCodesAggregate = [];
        foreach ($this->validators as $key => $validator) {
            $result = $validator->validate($validationSubject);
            if (!$result->isValid()) {
                $isValid = false;
                $failsDescriptionAggregate[] = $result->getFailsDescription();
                $errorCodesAggregate[] = $result->getErrorCodes();

                if (!empty($this->chainBreakingValidators[$key])) {
                    break;
                }
            }
        }

        return $this->createResult(
            $isValid,
            array_merge([], ...$failsDescriptionAggregate),
            array_merge([], ...$errorCodesAggregate)
        );
    }
}
