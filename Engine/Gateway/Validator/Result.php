<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Validator;

use Magento\Framework\Phrase;

class Result implements ResultInterface
{

    protected $isValid;

    /**
     * @var Phrase[]
     */
    protected $failsDescription;

    /**
     * @var string[]
     */
    protected $errorCodes;

    /**
     * @param bool $isValid
     * @param array $failsDescription
     * @param array $errorCodes
     */
    public function __construct(
        bool $isValid,
        array $failsDescription = [],
        array $errorCodes = []
    ) {
        $this->isValid = $isValid;
        $this->failsDescription = $failsDescription;
        $this->errorCodes = $errorCodes;
    }

    /**
     * @inheritDoc
     */
    public function isValid(): bool
    {
        return $this->isValid;
    }

    /**
     * @inheritDoc
     */
    public function getFailsDescription(): array
    {
        return $this->failsDescription;
    }

    /**
     * @inheritDoc
     */
    public function getErrorCodes(): array
    {
        return $this->errorCodes;
    }
}
