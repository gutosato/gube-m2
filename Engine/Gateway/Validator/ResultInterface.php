<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Validator;

use Magento\Framework\Phrase;

interface ResultInterface
{
    /**
     * Returns validation result
     *
     * @return bool
     */
    public function isValid(): bool;

    /**
     * Returns list of fails description
     *
     * @return Phrase[]
     */
    public function getFailsDescription(): array;

    /**
     * Returns list of error codes.
     *
     * @return string[]
     * @since 100.3.0
     */
    public function getErrorCodes(): array;
}
