<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Validator;

interface ValidatorInterface
{
    /**
     * @param $validationSubject
     * @return resultInterface
     */
    public function validate($validationSubject): resultInterface;
}
