<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Request;

use phpDocumentor\Reflection\Types\Mixed_;

interface BuilderInterface
{
    /**
     * @param array $buildSubject
     * @return mixed
     */
    public function build(array $buildSubject);
}
