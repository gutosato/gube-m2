<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Request;

use Magento\Framework\ObjectManager\TMap;
use Magento\Framework\ObjectManager\TMapFactory;

class BuilderComposite implements BuilderInterface
{

    /**
     * @var BuilderInterface[] | TMap
     */
    private $builders;

    public function __construct(
        TMapFactory $TMapFactory,
        array $builders = []
    ) {
        $this->builders = $TMapFactory->create(
            [
                'array' => $builders,
                'type' => BuilderInterface::class
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        $result = [];
        foreach ($this->builders as $builder) {
            $result = $this->merge($result, $builder->build($buildSubject));
        }

        return $result;
    }

    /**
     * @param array $result
     * @param array $builder
     * @return array
     */
    protected function merge(array $result, array $builder): array
    {
        return array_replace_recursive($result, $builder);
    }
}
