<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Response;

use Magento\Framework\ObjectManager\TMap;
use Magento\Framework\ObjectManager\TMapFactory;

class HandlerChain implements HandlerInterface
{

    private $handlers;

    /**
     * @param TMapFactory $TMapFactory
     * @param array $handlers
     */
    public function __construct(
        TMapFactory $TMapFactory,
        array $handlers = []
    ) {
        $this->handlers = $TMapFactory->create(
            [
                'array' => $handlers,
                'type' => HandlerInterface::class
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function handle(array $handlingSubject, $response): void
    {
        foreach ($this->handlers as $handler) {
            $handler->handle($handlingSubject, $response);
        }
    }
}
