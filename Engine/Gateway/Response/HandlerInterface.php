<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Response;

interface HandlerInterface
{

    /**
     * Handles response
     *
     * @param array $handlingSubject
     * @param $response
     * @return void
     */
    public function handle(array $handlingSubject, $response): void;
}
