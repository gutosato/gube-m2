<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Config;

use Gubee\Integration\Engine\Gateway\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Config\ReinitableConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Config implements ConfigInterface
{
    protected const DEFAULT_PATH_PATTERN = 'gubee_integration/%s/%s';

    protected const KEY_METHOD_CODE = 'general';

    protected $scopeConfig;

    protected $reinitableConfig;

    protected $methodCode;

    protected $pathPattern;

    protected $configWriter;

    /**
     * @param WriterInterface $configWriter
     * @param ScopeConfigInterface $scopeConfig
     * @param ReinitableConfigInterface $reinitableConfig
     * @param string|null $methodCode
     * @param string $pathPattern
     */
    public function __construct(
        WriterInterface $configWriter,
        ScopeConfigInterface $scopeConfig,
        ReinitableConfigInterface $reinitableConfig,
        string $methodCode = null,
        string $pathPattern = self::DEFAULT_PATH_PATTERN
    ) {
        $this->configWriter = $configWriter;
        $this->scopeConfig = $scopeConfig;
        $this->reinitableConfig = $reinitableConfig;
        $this->methodCode = $methodCode;
        $this->pathPattern = $pathPattern;
    }

    /**
     * @inheritDoc
     */
    public function setMethodCode(string $methodCode): void
    {
        $this->methodCode = $methodCode;
    }

    /**
     * @inheritDoc
     */
    public function setPathPattern(string $pathPattern): void
    {
        $this->pathPattern = $pathPattern;
    }

    /**
     * @inheritDoc
     */
    public function getValue(string $field, int $storeId = null)
    {
        if ($this->methodCode === null || $this->pathPattern === null) {
            return null;
        }

        return $this->scopeConfig->getValue(
            sprintf($this->pathPattern, $this->methodCode, $field),
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @inheritDoc
     */
    public function setValue(string $field, string $renewedToken, int $storeId = null)
    {
        if ($this->methodCode === null || $this->pathPattern === null) {
            return null;
        }
        $this->configWriter->save(
            sprintf($this->pathPattern, $this->methodCode, $field),
            $renewedToken
        );
        $this->reinitableConfig->reinit();
    }
}
