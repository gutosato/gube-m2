<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway;

interface CommandInterface
{

    /**
     * @param array $commandSubject
     * @return mixed
     */
    public function execute(array $commandSubject);
}
