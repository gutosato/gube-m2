<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway;

interface ConfigInterface
{

    /**
     * Sets method code
     *
     * @param string $methodCode
     * @return void
     */
    public function setMethodCode(string $methodCode): void;

    /**
     * Sets path pattern
     *
     * @param string $pathPattern
     * @return void
     */
    public function setPathPattern(string $pathPattern): void;

    /**
     * Retrieve information from payment configuration
     *
     * @param string $field
     * @param int|null $storeId
     *
     * @return mixed
     */
    public function getValue(string $field, int $storeId = null);

    /**
     * @param string $field
     * @param string $renewedToken
     * @param int|null $storeId
     * @return mixed
     */
    public function setValue(string $field, string $renewedToken, int $storeId = null);
}
