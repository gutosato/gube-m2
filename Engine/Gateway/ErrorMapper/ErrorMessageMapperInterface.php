<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\ErrorMapper;

interface ErrorMessageMapperInterface
{
    /**
     * @param string $code
     * @return mixed
     */
    public function getMessage(string $code);
}
