<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\ErrorMapper;

use Magento\Framework\Config\DataInterface;

class ErrorMessageMapper implements ErrorMessageMapperInterface
{

    protected $messageMapping;

    /**
     * @param DataInterface $messageMapping
     */
    public function __construct(DataInterface $messageMapping)
    {
        $this->messageMapping = $messageMapping;
    }

    /**
     * @inheritDoc
     */
    public function getMessage(string $code)
    {
        $message = $this->messageMapping->get($code);
        return $message ? __($message) : null;
    }
}
