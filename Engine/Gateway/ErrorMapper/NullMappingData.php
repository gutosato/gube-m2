<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\ErrorMapper;

use Magento\Framework\Config\DataInterface;

class NullMappingData implements DataInterface
{

    /**
     * @inheritDoc
     */
    public function get($key, $default = null)
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function merge(array $config)
    {
    }
}
