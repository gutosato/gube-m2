<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\ErrorMapper;

use Magento\Framework\Config\Data\Scoped;

class MappingData extends Scoped
{
    /**
     * @inheritdoc
     */
    protected $_scopePriorityScheme = ['global'];
}
