<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Http;

interface TransferFactoryInterface
{
    /**
     * @param $request
     * @return TransferInterface
     */
    public function create($request): TransferInterface;
}
