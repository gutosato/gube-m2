<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Http\Converter;

use Gubee\Integration\Engine\Gateway\Http\ConverterInterface;

class StringToArrayConverter implements ConverterInterface
{

    /**
     * @inheritDoc
     */
    public function convert($response): array
    {
        return json_decode($response);
    }
}
