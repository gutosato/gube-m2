<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Http;

use Gubee\Integration\Engine\Gateway\Http\ClientInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Gubee\Integration\Engine\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;

class Client implements ClientInterface
{

    private $clientFactory;

    private $logger;

    /**
     * Client constructor.
     * @param ZendClientFactory $clientFactory
     * @param Logger $logger
     */
    public function __construct(
        ZendClientFactory $clientFactory,
        Logger $logger
    ) {
        $this->clientFactory = $clientFactory;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     * @throws \Zend_Http_Client_Exception
     */
    public function placeRequest(TransferInterface $transferObject): array
    {
        $client = $this->clientFactory->create();

        $log = [
            "request_uri" => $transferObject->getUri(),
            "request" => $transferObject->getBody()
        ];

        $client->setUri($transferObject->getUri());
        $client->setHeaders($transferObject->getHeaders());
        $client->setMethod($transferObject->getMethod());
        $client->setParameterPost($transferObject->getBody());

        $result = [];
        try {
            $response = $client->request();
            $result = json_decode($response->getBody(), true);
        } catch (\Exception $exception) {
        } finally {
            $this->logger->debug($log);
        }

        return $result;
    }
}
