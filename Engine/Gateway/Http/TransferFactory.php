<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Http;

use Gubee\Integration\Engine\Gateway\Http\TransferBuilder;
use Magento\Framework\App\Request\Http as HttpRequest;

class TransferFactory implements TransferFactoryInterface
{

    protected $transferBuilder;

    /**
     * @param TransferBuilder $transferBuilder
     */
    public function __construct(
        TransferBuilder $transferBuilder
    ) {
        $this->transferBuilder = $transferBuilder;
    }

    /**
     * @inheritDoc
     */
    public function create($request): TransferInterface
    {
        return $this->transferBuilder
            ->setUri("")
            ->setHeaders(['Authorization' => ''])
            ->setMethod(HttpRequest::METHOD_POST)
            ->setBody($request)
            ->shouldEncode(false)
            ->build();
    }
}
