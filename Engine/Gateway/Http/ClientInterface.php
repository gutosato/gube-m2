<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Http;

interface ClientInterface
{
    /**
     * @param TransferInterface $transferObject
     * @return mixed
     */
    public function placeRequest(TransferInterface $transferObject);
}
