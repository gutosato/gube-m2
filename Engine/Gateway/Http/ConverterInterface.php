<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Gateway\Http;

interface ConverterInterface
{
    /**
     * @param mixed $response
     * @return array
     */
    public function convert($response): array;
}
