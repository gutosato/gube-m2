<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Model\Integration;

use Gubee\Integration\Engine\Model\Integration\IntegrationInterface;
use Gubee\Integration\Engine\Gateway\Command\CommandPoolInterface;

class Adapter implements IntegrationInterface
{

    private $code;

    private $storeId;

    protected $commandPool;

    /**
     * @param string $code
     * @param CommandPoolInterface|null $commandPool
     */
    public function __construct(
        string $code,
        CommandPoolInterface $commandPool = null
    ) {
        $this->code = $code;
        $this->commandPool = $commandPool;
    }

    /**
     * @param string $commandCode
     * @param array $arguments
     * @return mixed
     */
    public function executeCommand(string $commandCode, array $arguments = [])
    {
        if ($this->commandPool === null) {
            throw new \DomainException("The command pool isn't configured for use.");
        }

        $command = $this->commandPool->get($commandCode);

        return $command->execute($arguments);
    }

    /**
     * @param int $storeId
     */
    public function setStore(int $storeId)
    {
        $this->storeId = $storeId;
    }

    /**
     * @return int
     */
    public function getStore(): int
    {
        return $this->storeId;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    function execute(string $command, array $args = []): void
    {
        $this->executeCommand($command);
    }
}
