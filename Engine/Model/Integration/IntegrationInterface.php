<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Model\Integration;

interface IntegrationInterface
{
    function execute(string $command, array $args = []): void;
}
