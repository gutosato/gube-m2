<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Gubee\Integration\Engine\Model\Integration\IntegrationInterface;
use Gubee\Integration\Engine\Model\Factory;
use Magento\Framework\Exception\LocalizedException;

class Integration implements IntegrationInterface
{

    protected const XML_PATH_INTEGRATION_METHODS = 'gubee_integration';

    protected $scopeConfig;

    protected $objectManager;

    protected $factory;

    protected $code;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ObjectManagerInterface $objectManager
     * @param \Gubee\Integration\Engine\Model\Factory $factory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ObjectManagerInterface $objectManager,
        Factory $factory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->objectManager = $objectManager;
        $this->factory = $factory;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @param string $code
     * @return string
     */
    protected function getMethodModelConfigName(string $code): string
    {
        return sprintf('%s/%s/model', self::XML_PATH_INTEGRATION_METHODS, $code);
    }

    /**
     * @return IntegrationInterface
     * @throws LocalizedException
     */
    public function getMethodInstance(): IntegrationInterface
    {
        $className = $this->scopeConfig->getValue(
            $this->getMethodModelConfigName($this->code),
            ScopeInterface::SCOPE_STORE
        );
        return $this->factory->create($className, []);
    }

    /**
     * @param string $command
     * @throws LocalizedException
     */
    public function execute(string $command, array $args = []): void
    {
        $this->getMethodInstance()->execute($command);
    }
}
