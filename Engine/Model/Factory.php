<?php

declare(strict_types=1);

namespace Gubee\Integration\Engine\Model;

use Gubee\Integration\Engine\Model\Integration\IntegrationInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;

class Factory
{

    protected $objectManager;

    /**
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param string $className
     * @param array $data
     * @return IntegrationInterface
     * @throws LocalizedException
     */
    public function create(string $className, array $data = []): IntegrationInterface
    {
        $instance = $this->objectManager->create($className, $data);
        if (!$instance instanceof IntegrationInterface) {
            throw new LocalizedException(
                __(
                    '%1 class doesn\'t implement \Gubee\Integration\Engine\Model\Integration\IntegrationInterface',
                    $className
                )
            );
        }

        return $instance;
    }
}
