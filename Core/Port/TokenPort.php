<?php

declare(strict_types=1);

namespace Gubee\Integration\Core\Port;

interface TokenPort
{
    /**
     * @param string $token
     * @return string
     */
    public function apiToken(string $token): string;
}