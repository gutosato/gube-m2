<?php

declare(strict_types=1);

namespace Gubee\Integration\Core\Command\Token;

class RenewToken
{
    public function execute($token, $url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $token
        ]);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}