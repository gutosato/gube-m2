# Gubee Integration
## Descrição
- Módulo desenvolvido fazer integração como o marketplace hub da Gubee

## Instalacao
- Para fazer a instalação do modulo basta executar os seguintes comandos abaixo.
```ssh
composer config repositories.gubee-integration git "git@bitbucket.org:gutosato/gube-m2.git"
composer require gubee/module-integration
```
