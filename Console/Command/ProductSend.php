<?php

/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gubee\Integration\Console\Command;

use Gubee\Integration\Command\Product\SendProduct;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;



class ProductSend extends Command
{

    protected $commandSendProduct;

    public function __construct(
        SendProduct $commandSendProduct,
        $name = null
    ) {
        $this->commandSendProduct = $commandSendProduct;
        parent::__construct($name);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->commandSendProduct->execute();
        $output->writeln('Product Sent');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("gubee_integration:product_send");
        $this->setDescription("Send all products to gubee");
        parent::configure();
    }
}

