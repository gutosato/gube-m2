<?php

/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gubee\Integration\Console\Command;

use Gubee\Integration\Command\Brand\SendBrand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;


class BrandSend extends Command
{

    protected $commandSendBrand;

    public function __construct(
        SendBrand $commandSendBrand,
        $name = null
    ) {
        $this->commandSendBrand = $commandSendBrand;
        parent::__construct($name);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->commandSendBrand->execute();
        $output->writeln('Brand Sent');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("gubee_integration:brand_send");
        $this->setDescription("Send all Brand to gubee");
        parent::configure();
    }
}

