<?php

/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gubee\Integration\Console\Command;

use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Gubee\Integration\Model\Integration\GubeeInterface;
use Gubee\Integration\Engine\Model\Integration;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RenewToken extends Command
{
    protected $gubee;

    protected $integration;

    /**
     * @param GubeeInterface $gubee
     * @param Integration $integration
     * @param null $name
     */
    public function __construct(
        GubeeInterface $gubee,
        Integration $integration,
        $name = null
    ) {
        parent::__construct($name);
        $this->gubee = $gubee;
        $this->integration = $integration;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->integration->setCode('gubee');
        $this->integration->getMethodInstance()->renewToken();
        $output->writeln('Token Renewed');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("gubee_integration:renew_token");
        $this->setDescription("Renew Token");
        parent::configure();
    }
}

