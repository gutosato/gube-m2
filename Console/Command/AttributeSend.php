<?php

/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gubee\Integration\Console\Command;

use Gubee\Integration\Command\Attribute\SendAttribute;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;


class AttributeSend extends Command
{

    protected $commandSendAttribute;

    public function __construct(
        SendAttribute $commandSendAttribute,
        $name = null
    ) {
        $this->commandSendAttribute = $commandSendAttribute;
        parent::__construct($name);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->commandSendAttribute->execute();
        $output->writeln('Attribute Sent');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("gubee_integration:attribute_send");
        $this->setDescription("Send all attributes to gubee");
        parent::configure();
    }
}

