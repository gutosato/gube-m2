<?php

/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gubee\Integration\Console\Command;

use Gubee\Integration\Command\Category\SendCategory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;


class CategorySend extends Command
{

    protected $commandSendCategory;

    public function __construct(
        SendCategory $commandSendCategory,
        $name = null
    ) {
        $this->commandSendCategory = $commandSendCategory;
        parent::__construct($name);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->commandSendCategory->execute();
        $output->writeln('Category Sent');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("gubee_integration:category_send");
        $this->setDescription("Send all Category to gubee");
        parent::configure();
    }
}

