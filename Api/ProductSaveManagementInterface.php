<?php

/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gubee\Integration\Api;

interface ProductSaveManagementInterface
{

    /**
     * POST for ProductSave api
     * @param string $param
     * @return string
     */
    public function postProductSave($param);
}

