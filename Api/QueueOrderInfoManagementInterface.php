<?php

/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gubee\Integration\Api;

interface QueueOrderInfoManagementInterface
{

    /**
     * GET for QueueOrderInfo api
     * @param string $param
     * @return string
     */
    public function getQueueOrderInfo($param);
}

