<?php

/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gubee\Integration\Api;

interface AttributeUpdateManagementInterface
{

    /**
     * PUT for AttributeUpdate api
     * @param string $param
     * @return string
     */
    public function putAttributeUpdate($param);
}

