<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Gubee\Integration\Controller\Adminhtml\Sync;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Gubee\Integration\Command\Brand\SendBrand;
use Psr\Log\LoggerInterface;
use Exception;

class SendAllBrands extends Action
{

    protected $resultJsonFactory;

    protected $command;

    protected $logger;


    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        SendBrand $command,
        LoggerInterface $logger
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->command = $command;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Collect relations data
     *
     * @return Json
     */
    public function execute(): Json
    {
        try {
            $this->command->execute();
        } catch (Exception $e) {
            $this->logger->critical($e);
        }
        $result = $this->resultJsonFactory->create();
        return $result->setData(['success' => true]);
    }
}

