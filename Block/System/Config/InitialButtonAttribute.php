<?php
declare(strict_types=1);

namespace Gubee\Integration\Block\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;

class InitialButtonAttribute extends Field
{
    protected $_template = 'Gubee_Integration::system/config/buttonAttribute.phtml';
    public function __construct(Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function render(AbstractElement $element): string
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }
    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }
    public function getAjaxUrl(): string
    {
        return $this->getUrl('gubee/sync/sendallattributes');
    }

    public function getButtonHtml(): string
    {
        $button = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')->setData(['id' => 'btn_attribute_send', 'label' => __('Send Attributes'),]);
        return $button->toHtml();
    }

}