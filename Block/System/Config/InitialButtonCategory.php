<?php
declare(strict_types=1);

namespace Gubee\Integration\Block\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;

class InitialButtonCategory extends Field
{
    protected $_template = 'Gubee_Integration::system/config/buttonCategory.phtml';
    public function __construct(Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function render(AbstractElement $element): string
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }
    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }
    public function getAjaxUrl(): string
    {
        return $this->getUrl('gubee/sync/sendallcategories');
    }

    public function getButtonHtml(): string
    {
        $button = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')->setData(['id' => 'btn_category_send', 'label' => __('Send Categories'),]);
        return $button->toHtml();
    }

}