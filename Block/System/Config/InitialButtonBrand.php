<?php
declare(strict_types=1);

namespace Gubee\Integration\Block\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;

class InitialButtonBrand extends Field
{
    protected $_template = 'Gubee_Integration::system/config/buttonBrand.phtml';
    public function __construct(Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function render(AbstractElement $element): string
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }
    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }
    public function getAjaxUrl(): string
    {
        return $this->getUrl('gubee/sync/sendallbrands');
    }

    public function getButtonHtml(): string
    {
        $button = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')->setData(['id' => 'btn_brand_send', 'label' => __('Send Brands'),]);
        return $button->toHtml();
    }

}