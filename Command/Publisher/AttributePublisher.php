<?php

declare(strict_types=1);

namespace Gubee\Integration\Command\Publisher;

use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\Eav\Api\Data\AttributeInterface;

class AttributePublisher
{
    protected const TOPIC_NAME = 'gubee.integration.attribute';

    protected $publisher;

    /**
     * @param PublisherInterface $publisher
     */
    public function __construct(
        PublisherInterface $publisher
    ) {
        $this->publisher = $publisher;
    }

    /**
     * @param AttributeInterface $attribute
     */
    public function execute(AttributeInterface $attribute)
    {
        $this->publisher->publish(self::TOPIC_NAME, $attribute);
    }
}
