<?php

declare(strict_types=1);

namespace Gubee\Integration\Command\Publisher;

use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\Catalog\Model\Category;

class CategoryPublisher
{
    protected const TOPIC_NAME = 'gubee.integration.category';

    protected $publisher;

    /**
     * @param PublisherInterface $publisher
     */
    public function __construct(
        PublisherInterface $publisher
    ) {
        $this->publisher = $publisher;
    }

    /**
     * @param Category $category
     */
    public function execute(Category $category)
    {
        $this->publisher->publish(self::TOPIC_NAME, $category->getId());
    }
}
