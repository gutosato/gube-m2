<?php

declare(strict_types=1);

namespace Gubee\Integration\Command\Publisher;

use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\Catalog\Api\Data\ProductInterface;

class ProductPublisher
{
    protected const TOPIC_NAME = 'gubee.integration.product';

    protected $publisher;

    /**
     * @param PublisherInterface $publisher
     */
    public function __construct(
        PublisherInterface $publisher
    ) {
        $this->publisher = $publisher;
    }

    /**
     * @param ProductInterface $product
     */
    public function execute(ProductInterface $product)
    {
        $this->publisher->publish(self::TOPIC_NAME, $product);
    }
}
