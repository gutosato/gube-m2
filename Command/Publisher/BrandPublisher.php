<?php

declare(strict_types=1);

namespace Gubee\Integration\Command\Publisher;

use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\Eav\Api\Data\AttributeOptionInterface;


class BrandPublisher
{
    protected const TOPIC_NAME = 'gubee.integration.brand';

    protected $publisher;

    /**
     * @param PublisherInterface $publisher
     */
    public function __construct(
        PublisherInterface $publisher

    ) {
        $this->publisher = $publisher;
    }

    /**
     * @param AttributeOptionInterface $option
     */
    public function execute(AttributeOptionInterface $option)
    {
        $this->publisher->publish(self::TOPIC_NAME, $option);
    }
}
