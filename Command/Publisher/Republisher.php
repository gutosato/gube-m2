<?php
declare(strict_types=1);
namespace Gubee\Integration\Command\Publisher;

use Gubee\Integration\Command\Publisher\AttributePublisher;
use Gubee\Integration\Command\Publisher\CategoryPublisher;
use Gubee\Integration\Command\Publisher\BrandPublisher;
use Gubee\Integration\Command\Publisher\ProductPublisher;

class Republisher
{

    protected $attributePublisher;

    protected $categoryPublisher;

    protected $brandPublisher;

    protected $productPublisher;

    public function __construct(
        AttributePublisher $attributePublisher,
        CategoryPublisher $categoryPublisher,
        BrandPublisher $brandPublisher,
        ProductPublisher $productPublisher
    ) {
        $this->attributePublisher = $attributePublisher;
        $this->categoryPublisher = $categoryPublisher;
        $this->brandPublisher = $brandPublisher;
        $this->productPublisher = $productPublisher;
    }

    public function execute($republisherObj)
    {
        if (array_key_exists('attribute', $republisherObj)) {
            $this->attributePublisher->execute($republisherObj['attribute']);
        }elseif (array_key_exists('category', $republisherObj)){
            $this->categoryPublisher->execute($republisherObj['category']);
        }elseif (array_key_exists('brand', $republisherObj)){
            $this->brandPublisher->execute($republisherObj['brand']);
        }elseif (array_key_exists('product', $republisherObj)){
            $this->productPublisher->execute($republisherObj['product']);
        }
    }
}

