<?php
declare(strict_types=1);
namespace Gubee\Integration\Command\Category;

use Gubee\Integration\Command\Publisher\CategoryPublisher;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
class SendCategory
{
    protected $categoryCollectionFactory;

    protected $publisher;

    /**
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param CategoryPublisher $publisher
     */
    public function __construct(
        CategoryCollectionFactory $categoryCollectionFactory,
        CategoryPublisher $publisher
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->publisher = $publisher;
    }

    /**
     *
     */
    public function execute(): void
    {
        $categories = $this->categoryCollectionFactory->create()->addAttributeToSort('path');
        /** @var Category $category */

        foreach ($categories as $category) {
            $this->publisher->execute($category);
        }

    }
}