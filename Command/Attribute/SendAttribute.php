<?php
declare(strict_types=1);
namespace Gubee\Integration\Command\Attribute;

use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use Gubee\Integration\Command\Publisher\AttributePublisher;
use Magento\Eav\Api\Data\AttributeInterface;
use Gubee\Integration\Gateway\Config;

class SendAttribute
{
    protected $attributeCollectionFactory;

    protected $publisher;

    protected $config;

    public function __construct(
        CollectionFactory $attributeCollectionFactory,
        AttributePublisher $publisher,
        Config $config
    ) {
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->publisher = $publisher;
        $this->config = $config;
    }

    public function execute(): void
    {
        $attributes = $this->attributeCollectionFactory->create();
        if ($this->config->getBrandAttribute()){
            $attributes = $attributes->addFieldToFilter(
            'attribute_code',
            ['neq' => $this->config->getBrandAttribute()])
        ;
        }

        foreach ($attributes as $attribute) {
            $this->publisher->execute($attribute);
        }
    }
}