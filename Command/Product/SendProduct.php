<?php
declare(strict_types=1);
namespace Gubee\Integration\Command\Product;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Gubee\Integration\Command\Publisher\ProductPublisher;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;

class SendProduct
{
    protected $productCollectionFactory;

    protected $publisher;

    public function __construct(
        CollectionFactory $productCollectionFactory,
        ProductPublisher $publisher
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->publisher = $publisher;
    }

    public function execute(): void
    {
        $products = $this->productCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter(ProductInterface::STATUS, Status::STATUS_ENABLED )
            ;
        foreach ($products as $product) {
            $this->publisher->execute($product);
        }
    }
}