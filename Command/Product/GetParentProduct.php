<?php

declare(strict_types=1);

namespace Gubee\Integration\Command\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Catalog\Api\ProductRepositoryInterface;

class GetParentProduct
{
    protected $typeConfigurable;

    protected $productRepository;

    public function __construct(
        Configurable $typeConfigurable,
        ProductRepositoryInterface $productRepository
    ) {
        $this->typeConfigurable = $typeConfigurable;
        $this->productRepository = $productRepository;
    }

    public function execute(ProductInterface $simpleProduct)
    {
        $parentsProduct = $this->typeConfigurable->getParentIdsByChild($simpleProduct->getId());
        if (count($parentsProduct)) {
            return $this->productRepository->getById($parentsProduct[0]);
        }
    }
}