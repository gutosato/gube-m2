<?php

declare(strict_types=1);

namespace Gubee\Integration\Command;

use Gubee\Integration\Engine\Model\Integration;
use Magento\Framework\Exception\LocalizedException;

class RenewToken
{

    protected $integration;

    /**
     * @param Integration $integration
     */
    public function __construct(
        Integration $integration
    ) {
        $this->integration = $integration;
    }

    /**
     * @throws LocalizedException
     */
    public function execute()
    {
        $this->integration->setCode('gubee');
        $this->integration->getMethodInstance()->renewToken();
    }
}
