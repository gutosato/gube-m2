<?php

declare(strict_types=1);

namespace Gubee\Integration\Command\Brand;

use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use Gubee\Integration\Command\Publisher\BrandPublisher;
use Gubee\Integration\Gateway\Config;
use Magento\Eav\Api\Data\AttributeInterface;

class SendBrand
{
    protected $attributeCollectionFactory;

    protected $publisher;

    protected $config;

    public function __construct(
        CollectionFactory $attributeCollectionFactory,
        BrandPublisher $publisher,
        Config $config
    ) {
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->publisher = $publisher;
        $this->config = $config;
    }

    public function execute(): void
    {
        /** @var AttributeInterface $attribute */
        $attribute = $this->attributeCollectionFactory->create()->addFieldToFilter(
            'attribute_code',
            $this->config->getBrandAttribute()
        )->getFirstItem();
        if (count($attribute->getOptions())){
            foreach ($attribute->getOptions() as $option) {
                $this->publisher->execute($option);
            }
        }
    }
}