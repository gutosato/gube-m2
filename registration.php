<?php
/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Gubee_Integration', __DIR__);

