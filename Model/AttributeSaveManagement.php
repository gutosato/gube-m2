<?php

/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gubee\Integration\Model;

class AttributeSaveManagement implements \Gubee\Integration\Api\AttributeSaveManagementInterface
{

    /**
     * {@inheritdoc}
     */
    public function postAttributeSave($param)
    {
        return 'hello api POST return the $param ' . $param;
    }
}

