<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\Integration;


interface GubeeInterface
{
    /**
     *
     */
    public function renewToken(): void;

    /**
     * @param array $args
     */
    public function selectAttribute(array $args): void;

    /**
     * @param array $args
     */
    public function saveAttribute(array $args): void;

    /**
     * @param array $args
     */
    public function updateAttribute(array $args): void;

    /**
     * @param array $args
     */
    public function selectCategory(array $args): void;

    /**
     * @param array $args
     */
    public function saveCategory(array $args): void;

    /**
     * @param array $args
     */
    public function updateCategory(array $args): void;

    /**
     * @param array $args
     */
    public function selectBrand(array $args): void;

    /**
     * @param array $args
     */
    public function saveBrand(array $args): void;

    /**
     * @param array $args
     */
    public function updateBrand(array $args): void;

    /**
     * @param array $args
     */
    public function selectProduct(array $args): void;

    /**
     * @param array $args
     */
    public function saveProduct(array $args): void;

    /**
     * @param array $args
     */
    public function updateProduct(array $args): void;

    /**
     * @param array $args
     */
    public function deleteProduct(array $args): void;
}
