<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\Integration;

use Gubee\Integration\Engine\Model\Integration\IntegrationInterface;
use Gubee\Integration\Model\Integration\GubeeInterface;

class Adapter implements IntegrationInterface, GubeeInterface
{

    private $code = 'gubee';

    protected $adapter;

    /**
     * @param IntegrationInterface $adapter
     */
    public function __construct(
        IntegrationInterface $adapter
    ) {
        $this->adapter = $adapter;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->adapter->isActive();
    }

    /**
     * @param string $command
     * @param array $args
     */
    public function execute(string $command, array $args = []): void
    {
        $this->adapter->executeCommand($command, $args);
    }

    /**
     *
     */
    public function renewToken(): void
    {
        $this->execute('renew_token');
    }

    /**
     * @param array $args
     */
    public function selectCategory(array $args): void
    {
        $this->execute('select_category', $args);
    }

    /**
     * @param array $args
     */
    public function saveCategory(array $args): void
    {
        $this->execute('save_category', $args);
    }

    /**
     * @param array $args
     */
    public function updateCategory(array $args): void
    {
        $this->execute('update_category', $args);
    }

    /**
     * @param array $args
     */
    public function selectAttribute(array $args): void
    {
        $this->execute('select_attribute', $args);
    }

    /**
     * @param array $args
     */
    public function saveAttribute(array $args): void
    {
        $this->execute('save_attribute', $args);
    }

    /**
     * @param array $args
     */
    public function updateAttribute(array $args): void
    {
        $this->execute('update_attribute', $args);
    }

    /**
     * @param array $args
     */
    public function selectBrand(array $args): void
    {
        $this->execute('select_brand', $args);
    }

    /**
     * @param array $args
     */
    public function saveBrand(array $args): void
    {
        $this->execute('save_brand', $args);
    }

    /**
     * @param array $args
     */
    public function updateBrand(array $args): void
    {
        $this->execute('update_brand', $args);
    }

    /**
     * @param array $args
     */
    public function selectProduct(array $args): void
    {
        $this->execute('select_product', $args);
    }

    /**
     * @param array $args
     */
    public function saveProduct(array $args): void
    {
        $this->execute('save_product', $args);
    }

    /**
     * @param array $args
     */
    public function updateProduct(array $args): void
    {
        $this->execute('update_product', $args);
    }

    /**
     * @param array $args
     */
    public function deleteProduct(array $args): void
    {
        $this->execute('delete_product', $args);
    }
}
