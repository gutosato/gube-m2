<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\Config\Source;

use Magento\Catalog\Model\Config;
use \Magento\Framework\Data\OptionSourceInterface;

class UnitTimeType implements OptionSourceInterface
{

    protected $config;

    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    public function toOptionArray(): array
    {
        $warrantyTypeArray = [
            [
                'label' => __('Please Select'),
                'value' => ''
            ],
            [
                'label' => __('Months'),
                'value' => 'MONTH'
            ],
            [
                'label' => __('Days'),
                'value' => 'DAYS'
            ]

        ];

        return $warrantyTypeArray;
    }

}