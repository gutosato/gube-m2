<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\Config\Source;

use Magento\Catalog\Model\Config;
use Magento\Catalog\Model\Product;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;

class AttSource implements OptionSourceInterface
{

    protected $config;

    protected $attributeFactory;

    public function __construct(
        CollectionFactory $attributeFactory,
        Config $config
    ) {
        $this->attributeFactory = $attributeFactory;
        $this->config = $config;
    }

    public function toOptionArray(): array
    {
        $attributes = $this->attributeFactory->create();


        $attributesArray = array(
            array(
                'label' => __('Please Select'),
                'value' => ''
            )

        );
        foreach ($attributes as $attribute) {
            $attributesArray[$attribute->getFrontendLabel()] = array(
                'value' => $attribute->getAttributeCode(),
                'label' => $attribute->getFrontendLabel()
            );
        }
        ksort($attributesArray);
        return $attributesArray;
    }

}