<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\Consumer;

use Gubee\Integration\Model\Integration\GubeeInterface;
use Gubee\Integration\Engine\Model\Integration;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Api\Data\ProductInterface;

class ProductConsumer
{
    protected $integration;

    protected $gubee;

    public function __construct(
        Integration $integration,
        GubeeInterface $gubee
    ) {
        $this->integration = $integration;
        $this->gubee = $gubee;
    }

    /**
     * @param ProductInterface $product
     * @throws LocalizedException
     */
    public function execute(ProductInterface $product): void
    {
        $this->integration->setCode('gubee');
        $this->integration->getMethodInstance()->selectProduct(['product' => $product]);
    }


}