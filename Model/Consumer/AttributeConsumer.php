<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\Consumer;

use Gubee\Integration\Model\Integration\GubeeInterface;
use Gubee\Integration\Engine\Model\Integration;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Framework\Exception\LocalizedException;

class AttributeConsumer
{
    protected $integration;

    protected $gubee;

    public function __construct(
        Integration $integration,
        GubeeInterface $gubee
    ) {
        $this->integration = $integration;
        $this->gubee = $gubee;
    }

    /**
     * @param AttributeInterface $attribute
     * @throws LocalizedException
     */
    public function execute(AttributeInterface $attribute): void
    {
        $this->integration->setCode('gubee');
        $this->integration->getMethodInstance()->selectAttribute(['attribute' => $attribute]);
    }


}