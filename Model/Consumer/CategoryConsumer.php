<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\Consumer;

use Gubee\Integration\Model\Integration\GubeeInterface;
use Gubee\Integration\Engine\Model\Integration;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;

use Magento\Framework\Exception\LocalizedException;

class CategoryConsumer
{
    protected $integration;

    protected $gubee;

    protected $categoryCollectionFactory;

    /**
     * @param Integration $integration
     * @param GubeeInterface $gubee
     */
    public function __construct(
        Integration $integration,
        GubeeInterface $gubee,
        CategoryCollectionFactory $categoryCollectionFactory
    ) {
        $this->integration = $integration;
        $this->gubee = $gubee;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    /**
     * @param string $categoryId
     * @throws LocalizedException
     */
    public function execute(string $categoryId): void
    {
        $category = $this->categoryCollectionFactory->create()->addAttributeToSelect('*')->addIdFilter(
            $categoryId
        )->getFirstItem();
        $this->integration->setCode('gubee');
        $this->integration->getMethodInstance()->selectCategory(['category' => $category]);
    }

}