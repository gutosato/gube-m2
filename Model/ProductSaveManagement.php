<?php

/**
 * Copyright © Gubee All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Gubee\Integration\Model;

class ProductSaveManagement implements \Gubee\Integration\Api\ProductSaveManagementInterface
{

    /**
     * {@inheritdoc}
     */
    public function postProductSave($param)
    {
        return 'hello api POST return the $param ' . $param;
    }
}

