<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway;

use Magento\Catalog\Api\Data\ProductInterface;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\ConfigurableProduct\Api\Data\OptionValueInterfaceFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Gubee\Integration\Gateway\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Catalog\Model\Product\Gallery\ReadHandler as GalleryReadHandler;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\ConfigurableProduct\Model\OptionRepository;
use Gubee\Integration\Gateway\ProductBuilder;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;


class ProductVariantBuilder
{
    protected $config;

    protected $product;

    protected $productRepository;

    protected $scopeConfig;

    protected $galleryReadHandler;

    protected $stockState;

    protected $optionRepository;

    protected $typeConfigurable;

    public function __construct(
        Config $config,
        ProductRepositoryInterface $productRepository,
        ScopeConfigInterface $scopeConfig,
        GalleryReadHandler $galleryReadHandler,
        StockStateInterface $stockState,
        OptionRepository $optionRepository,
        Configurable $typeConfigurable
    ) {
        $this->config = $config;
        $this->productRepository = $productRepository;
        $this->scopeConfig = $scopeConfig;
        $this->galleryReadHandler = $galleryReadHandler;
        $this->stockState = $stockState;
        $this->optionRepository = $optionRepository;
        $this->typeConfigurable = $typeConfigurable;
    }

    public function getDimension(ProductInterface $product)
    {
        return [
            "depth" => [
                "type" => $this->getDimensionType(),
                "value" => $product->getData($this->config->getDepth())
            ],
            "height" => [
                "type" => $this->getDimensionType(),
                "value" => $product->getData($this->config->getHeight())
            ],
            "width" => [
                "type" => $this->getDimensionType(),
                "value" => $product->getData($this->config->getWidth())
            ],
            "weight" => [
                "type" => $this->getWeightType(),
                "value" => $product->getData($this->config->getWeight())
            ],

        ];
    }

    public function getDimensionType()
    {
        if ($this->scopeConfig->getValue("general/locale/weight_unit") === "kgs") {
            return "CENTIMETER";
        }
    }

    public function getWeightType()
    {
        if ($this->scopeConfig->getValue("general/locale/weight_unit") === "kgs") {
            return "KILOGRAM";
        }
    }

    public function getEan(ProductInterface $product)
    {
        return $product->getData($this->config->getEan());
    }

    public function getHandlingTime()
    {
        return [
            "type" => $this->config->getUnitTime(),
            "value" => $this->config->getHandlingTime()
        ];
    }

    public function getImages(ProductInterface $product)
    {
        $result = [];
        $this->galleryReadHandler->execute($product);
        $productImages = $product->getMediaGalleryImages();
        if (!$productImages->getSize()) {
            return;
        }
        foreach ($productImages as $productImage) {
            $image["name"] = $productImage->getFile();
            $image["order"] = $productImage->getPosition();
            $image["url"] = $productImage->getUrl();
            $result[] = $image;
        }
        return $result;
    }

    public function getMain(ProductInterface $product)
    {
        if ($product->getTypeId() == "configurable") {
            return true;
        } elseif ($product->getTypeId() == "simple") {
            $parentsProduct = $this->typeConfigurable->getParentIdsByChild($product->getId());
            if (count($parentsProduct)) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public function getPrices(ProductInterface $product)
    {
        return [
            [
                "type" => "DEFAULT",
                "value" => $product->getFinalPrice()
            ]
        ];
    }

    public function getStock(ProductInterface $product)
    {
        return [
            ["qty" => $this->stockState->getStockQty($product->getId())]
        ];
    }

    public function getWarrantyTime(ProductInterface $product)
    {
        return [
            "type" => $this->config->getUnitTime(),
            "value" => $this->config->getWarrantyTime()
        ];
    }

    public function getVariantSpecification(ProductInterface $product)
    {
        if ($product->getTypeId() == "simple") {
            $parentsProduct = $this->typeConfigurable->getParentIdsByChild($product->getId());
            if (count($parentsProduct)) {
                $parentProduct = $this->productRepository->getById($parentsProduct[0]);
                foreach ($parentProduct->getTypeInstance()->getConfigurableAttributes($parentProduct) as $attribute) {
                    $productAttribute = $attribute->getProductAttribute();
                    $attributeCode = $productAttribute->getAttributeCode();
                    $value = $productAttribute->getSource()->getOptionText($product->getData($attributeCode));
                    $result[] = [
                        "attribute" => $attribute->getLabel(),
                        "values" => [$value]
                    ];
                }
                return $result;
            } else {
                return [];
            }
        } elseif ($product->getTypeId() == "configurable") {
            foreach ($product->getTypeInstance()->getConfigurableAttributes($product) as $attribute) {
                $result[] = [
                    "attribute" => $attribute->getLabel(),
                    "values" => [""]
                ];
            }
            return $result;
        }
    }
}