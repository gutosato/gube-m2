<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Response\HandlerBrand;

use Gubee\Integration\Engine\Gateway\Response\HandlerInterface;
use Gubee\Integration\Gateway\Config;
use Gubee\Integration\Engine\Model\Integration;

/**
 * @deplacated
 */
class Get implements HandlerInterface
{

    protected $config;

    protected $integration;

    /**
     * @param Config $config
     */
    public function __construct(
        Config $config,
        Integration $integration
    ) {
        $this->config = $config;
        $this->integration = $integration;
    }

    /**
     * @inheritDoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function handle(array $handlingSubject, $response): void
    {
        $this->integration->setCode('gubee');
        if ($response->getStatus() == 200) {
            $this->integration->getMethodInstance()->updateBrand($handlingSubject);
        } else {
            $this->integration->getMethodInstance()->saveBrand($handlingSubject);
        }
    }
}
