<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Response\HandlerProduct;

use Gubee\Integration\Engine\Gateway\Response\HandlerInterface;
use Gubee\Integration\Gateway\Config;
use Gubee\Integration\Engine\Model\Integration;
use Magento\Framework\Exception\LocalizedException;

/**
 * @deplacated
 */
class Delete implements HandlerInterface
{

    protected $config;

    protected $integration;

    /**
     * @param Config $config
     */
    public function __construct(
        Config $config,
        Integration $integration
    ) {
        $this->config = $config;
        $this->integration = $integration;
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     */
    public function handle(array $handlingSubject, $response): void
    {

    }
}
