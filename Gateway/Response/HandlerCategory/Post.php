<?php
declare(strict_types=1);

namespace Gubee\Integration\Gateway\Response\HandlerCategory;

use Gubee\Integration\Engine\Gateway\Response\HandlerInterface;
use Gubee\Integration\Gateway\Config;

/**
 * @deplacated
 */
class Post implements HandlerInterface
{

    protected $config;

    /**
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    public function handle(array $handlingSubject, $response): void
    {

    }
}
