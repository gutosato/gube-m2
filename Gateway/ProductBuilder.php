<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway;

use Magento\Catalog\Api\Data\ProductInterface;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\Exception\NoSuchEntityException;
use Gubee\Integration\Gateway\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Gubee\Integration\Gateway\ProductVariantBuilder;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Gubee\Integration\Command\Product\GetParentProduct;

class ProductBuilder
{
    protected $config;

    protected $product;

    protected $productRepository;

    protected $scopeConfig;

    protected $variantProductBuilder;

    protected $typeConfigurable;

    protected $commandGetParent;

    public function __construct(
        Config $config,
        ProductRepositoryInterface $productRepository,
        ScopeConfigInterface $scopeConfig,
        ProductVariantBuilder $variantProductBuilder,
        Configurable $typeConfigurable,
        GetParentProduct $commandGetParent
    ) {
        $this->config = $config;
        $this->productRepository = $productRepository;
        $this->scopeConfig = $scopeConfig;
        $this->variantProductBuilder = $variantProductBuilder;
        $this->typeConfigurable = $typeConfigurable;
        $this->commandGetParent = $commandGetParent;
    }

    public function getBrand(ProductInterface $product)
    {
        if ($product->getData($this->config->getBrandAttribute())) {
            return $product->getData($this->config->getBrandAttribute());
        } else {
            return $this->config->getDefaultBrand();
        }
    }

    public function getCategories(ProductInterface $product): array
    {
        return $product->getCategoryIds();
    }

    public function getId(ProductInterface $product)
    {
        return $product->getSku();
    }

    public function getMainCategory(ProductInterface $product)
    {
        return $product->getCategoryCollection()->setOrder('path', 'DESC')->getFirstItem()->getId();
    }

    public function getMainSku(ProductInterface $product)
    {
        if ($product->getTypeId() == "simple") {
            $parentsProduct = $this->typeConfigurable->getParentIdsByChild($product->getId());
            if (count($parentsProduct)) {
                $parentProduct = $this->productRepository->getById($parentsProduct[0]);
                return $parentProduct->getSku();
            } else {
                return $product->getSku();
            }
        } elseif ($product->getTypeId() == "configurable") {
            return $product->getSku();
        }
    }

    public function getName(ProductInterface $product)
    {
        return $product->getName();
    }

    public function getNbm(ProductInterface $product)
    {
        return $product->getData($this->config->getNbm());
    }

    public function getOrigin(ProductInterface $product): string
    {
        return "NATIONAL";
    }

    public function getStatus(ProductInterface $product): string
    {
        return $product->getStatus() == Status::STATUS_ENABLED ? "ACTIVE" : "INACTIVE";
    }

    public function getType(ProductInterface $product)
    {
        if ($product->getTypeId() == "configurable") {
            return "VARIANT";
        } elseif ($product->getTypeId() == "simple") {
            $parentsProduct = $this->typeConfigurable->getParentIdsByChild($product->getId());
            if (count($parentsProduct)) {
                return "VARIANT";
            } else {
                return "SIMPLE";
            }
        }
    }

    public function getVariantAttributes(ProductInterface $product)
    {
        if ($product->getTypeId() == "simple") {
            $parentsProduct = $this->typeConfigurable->getParentIdsByChild($product->getId());
            if (count($parentsProduct)) {
                $parentProduct = $this->productRepository->getById($parentsProduct[0]);
                foreach ($parentProduct->getTypeInstance()->getConfigurableAttributes($parentProduct) as $attribute) {
                    $result[] = $attribute->getLabel();
                }
                return $result;
            } else {
                return [];
            }
        } elseif ($product->getTypeId() == "configurable") {
            foreach ($product->getTypeInstance()->getConfigurableAttributes($product) as $attribute) {
                $result[] = $attribute->getLabel();
            }
            return $result;
        }
    }

    public function getVariations(ProductInterface $product, $isRecursive = false)
    {
        if ($product->getTypeId() == "configurable") {
            $collection = $product->getTypeInstance()->getUsedProductCollection($product);
            $children = $collection->addFieldToSelect('*');
            foreach ($children as $child){
                $variations[] = $this->getVariations($child, true);
            }

        }
        $variation["cost"] = $product->getPrice();
        $variation["description"] = $product->getDescription();
        $variation["dimension"] = $this->variantProductBuilder->getDimension($product);
        $variation["ean"] = $this->variantProductBuilder->getEan($product);
        $variation["handlingTime"] = $this->variantProductBuilder->getHandlingTime();
        $variation["images"] = $this->variantProductBuilder->getImages($product);
        $variation["main"] = $this->variantProductBuilder->getMain($product);
        $variation["name"] = $this->getName($product);
        $variation["prices"] = $this->variantProductBuilder->getPrices($product);
        $variation["sku"] = $this->getId($product);
        $variation["skuId"] = $this->getId($product);
        $variation["status"] = $this->getStatus($product);
        $variation["stocks"] = $this->variantProductBuilder->getStock($product);
        $variation["variantSpecification"] = $this->variantProductBuilder->getVariantSpecification($product);
        $variation["warrantyTime"] = $this->variantProductBuilder->getWarrantyTime($product);
        if (!$isRecursive){
            $variations[] = $variation;
            return $variations;
        }
        return $variation;
    }


}