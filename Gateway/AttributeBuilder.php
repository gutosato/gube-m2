<?php
declare(strict_types=1);
namespace Gubee\Integration\Gateway;

use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class AttributeBuilder
{
    protected $productAttributeRepository;

    /**
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     */
    public function __construct(
        ProductAttributeRepositoryInterface $productAttributeRepository
    ) {
        $this->productAttributeRepository = $productAttributeRepository;
    }

    /**
     * @param AttributeInterface $attribute
     * @return string
     */
    public function getAttrType(AttributeInterface $attribute): string
    {
        switch ($attribute->getFrontendInput()) {
            case 'text':
            case 'price':
            default:
                $result = 'TEXT';
                break;
            case 'textarea':
                $result = 'TEXTAREA';
                break;
            case 'multiselect':
                $result = 'MULTISELECT';
                break;
            case 'select':
                $result = 'SELECT';
                break;
        };
        return $result;
    }

    /**
     * @param AttributeInterface $attribute
     * @return string
     */
    public function getId(AttributeInterface $attribute): string
    {
        return $attribute->getAttributeCode();
    }

    /**
     * @param AttributeInterface $attribute
     * @return string
     */
    public function getName(AttributeInterface $attribute): ?string
    {
        return $attribute->getStoreLabel();
    }

    /**
     * @param AttributeInterface $attribute
     * @return bool
     */
    public function getRequired(AttributeInterface $attribute): bool
    {
        return $attribute->getIsRequired();
    }

    /**
     * @param AttributeInterface $attribute
     * @return array
     */
    public function getOptions(AttributeInterface $attribute): array
    {
        $result = array();
        $attributeOptions = $attribute->getOptions();
        if (count($attributeOptions)) {
            foreach ($attribute->getOptions() as $option) {
                $result[] = $option->getLabel();
            }
        }
        return $result;
    }

    /**
     * @param AttributeInterface $attribute
     * @return bool
     * @throws NoSuchEntityException
     */
    public function getVariant(AttributeInterface $attribute): bool
    {
        $productAttribute = $this->productAttributeRepository->get($attribute->getAttributeCode());
        $result = false;
        if (in_array('configurable', $productAttribute->getApplyTo())) {
            $result = true;
        }
        return $result;
    }
}