<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Http;

use Gubee\Integration\Engine\Gateway\Http\TransferBuilder;
use Gubee\Integration\Engine\Gateway\Http\TransferFactoryInterface;
use Gubee\Integration\Engine\Gateway\Http\TransferInterface;
use Magento\Framework\App\Request\Http as HttpRequest;
use Gubee\Integration\Gateway\Config;

class TransferFactoryDelete implements TransferFactoryInterface
{
    protected $endPoint;

    protected $transferBuilder;

    protected $config;

    private $param;

    /**
     * @param TransferBuilder $transferBuilder
     * @param Registry $registry
     */
    public function __construct(
        string $endPoint,
        TransferBuilder $transferBuilder,
        Config $config
    ) {
        $this->endPoint = $endPoint;
        $this->transferBuilder = $transferBuilder;
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    public function create($request): TransferInterface
    {

        $this->param = $request['param'];
        $token = $this->config->getRenewedToken();

        return $this->transferBuilder
            ->setUri($this->config->getBaseUrl() . $this->endPoint. $this->param)
            ->setHeaders(
                [
                    'Authorization' => 'Bearer ' . $token,
                ]
            )
            ->setMethod(HttpRequest::METHOD_DELETE)
            ->setBody()
            ->shouldEncode(false)
            ->build();
    }
}
