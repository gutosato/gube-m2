<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Http;

use Gubee\Integration\Engine\Gateway\Http\TransferBuilder;
use Gubee\Integration\Engine\Gateway\Http\TransferFactoryInterface;
use Gubee\Integration\Engine\Gateway\Http\TransferInterface;
use Magento\Framework\App\Request\Http as HttpRequest;
use Gubee\Integration\Gateway\Config;

class TokenTransferFactory implements TransferFactoryInterface
{
    protected const KEY_API_TOKEN_URI = "tokens/revalidate/apitoken";

    protected $transferBuilder;

    protected $config;

    /**
     * @param TransferBuilder $transferBuilder
     * @param Config $config
     */
    public function __construct(
        TransferBuilder $transferBuilder,
        Config $config
    ) {
        $this->transferBuilder = $transferBuilder;
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    public function create($request): TransferInterface
    {
        return $this->transferBuilder
            ->setUri($this->config->getBaseUrl() . self::KEY_API_TOKEN_URI)
            ->setMethod(HttpRequest::METHOD_POST)
            ->setBody($request)
            ->shouldEncode(false)
            ->build();
    }
}
