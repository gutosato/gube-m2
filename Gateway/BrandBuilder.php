<?php
declare(strict_types=1);
namespace Gubee\Integration\Gateway;

use Magento\Eav\Api\Data\AttributeOptionInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class BrandBuilder
{
    /**
     * @param AttributeOptionInterface $option
     * @return string
     */
    public function getId(AttributeOptionInterface $option): string
    {
        return $option->getValue();
    }

    /**
     * @param AttributeOptionInterface $option
     * @return string
     */
    public function getName(AttributeOptionInterface $option): ?string
    {
        return $option->getLabel();
    }

    /**
     * @param AttributeOptionInterface $option
     * @return string
     */
    public function getDescription(AttributeOptionInterface $option): ?string
    {
        return $option->getLabel();
    }


}