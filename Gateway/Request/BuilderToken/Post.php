<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Request\BuilderToken;

use Gubee\Integration\Engine\Gateway\Request\BuilderInterface;
use Gubee\Integration\Gateway\Config;

class Post implements BuilderInterface
{
    protected $config;

    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject)
    {
        return $this->config->getToken();
    }
}
