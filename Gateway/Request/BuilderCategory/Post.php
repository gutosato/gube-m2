<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Request\BuilderCategory;

use Gubee\Integration\Engine\Gateway\Request\BuilderInterface;
use Gubee\Integration\Gateway\Config;
use Gubee\Integration\Gateway\CategoryBuilder;

class Post implements BuilderInterface
{
    protected $config;

    protected $categoryBuilder;

    public function __construct(
        Config $config,
        CategoryBuilder $categoryBuilder
    ) {
        $this->config = $config;
        $this->categoryBuilder = $categoryBuilder;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        $category = $buildSubject['category'];
        $result = [
              "active" => $this->categoryBuilder->getActive($category),
              "description" => $this->categoryBuilder->getDescription($category),
              "id" => $this->categoryBuilder->getId($category),
              "name" => $this->categoryBuilder->getName($category)
        ];
        if ($this->categoryBuilder->getParent($category)){
            $result["parent"] = $this->categoryBuilder->getParent($category);
        }


        return $result;
    }
}
