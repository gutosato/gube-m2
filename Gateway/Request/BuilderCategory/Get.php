<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Request\BuilderCategory;

use Gubee\Integration\Engine\Gateway\Request\BuilderInterface;
use Gubee\Integration\Gateway\Config;
use Gubee\Integration\Gateway\CategoryBuilder;

class Get implements BuilderInterface
{
    protected $config;

    protected $categoryBuilder;

    public function __construct(
        Config $config,
        CategoryBuilder $categoryBuilder
    ) {
        $this->config = $config;
        $this->categoryBuilder = $categoryBuilder;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        $category = $buildSubject['category'];
        $result = [
              "param" => $this->categoryBuilder->getId($category),
        ];
        return $result;
    }
}
