<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Request\BuilderProduct;

use Gubee\Integration\Engine\Gateway\Request\BuilderInterface;
use Gubee\Integration\Gateway\Config;
use Gubee\Integration\Gateway\ProductBuilder;
use Gubee\Integration\Command\Product\GetParentProduct;

class Get implements BuilderInterface
{
    protected $config;

    protected $productBuilder;

    protected $commandGetParent;

    public function __construct(
        Config $config,
        ProductBuilder $productBuilder,
        GetParentProduct $commandGetParent

    ) {
        $this->config = $config;
        $this->productBuilder = $productBuilder;
        $this->commandGetParent = $commandGetParent;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        /** @var \Magento\Catalog\Api\Data\ProductInterface $product */
        $product = $buildSubject['product'];
        if ($product->getTypeId() == "simple") {
            $parentProduct = $this->commandGetParent->execute($product);
            if ($parentProduct && $parentProduct->getId()) {
                $product = $parentProduct;
            }
        }
        $result["param"] = $this->productBuilder->getId($product);
        return $result;
    }
}
