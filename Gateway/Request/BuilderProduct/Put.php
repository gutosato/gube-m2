<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Request\BuilderProduct;

use Gubee\Integration\Engine\Gateway\Request\BuilderInterface;
use Gubee\Integration\Gateway\Config;
use Gubee\Integration\Gateway\ProductBuilder;
use Gubee\Integration\Command\Product\GetParentProduct;

class Put implements BuilderInterface
{
    protected $config;

    protected $productBuilder;

    protected $commandGetParent;

    public function __construct(
        Config $config,
        ProductBuilder $productBuilder,
        GetParentProduct $commandGetParent
    ) {
        $this->config = $config;
        $this->productBuilder = $productBuilder;
        $this->commandGetParent = $commandGetParent;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        $product = $buildSubject['product'];
        /** @var \Magento\Catalog\Api\Data\ProductInterface $product */
        $result["param"] = $this->productBuilder->getId($product);
        if ($product->getTypeId() == "simple") {
            $parentProduct = $this->commandGetParent->execute($product);
            if ($parentProduct && $parentProduct->getId()) {
                $result["param"] = $this->productBuilder->getId($parentProduct);
            }
        }
        $result["brand"] = $this->productBuilder->getBrand($product);
        $result["categories"] = $this->productBuilder->getCategories($product);
        $result["id"] = $this->productBuilder->getId($product);
        $result["mainCategory"] = $this->productBuilder->getMainCategory($product);
        $result["mainSku"] = $this->productBuilder->getMainSku($product);
        $result["name"] = $this->productBuilder->getName($product);
        $result["nbm"] = $this->productBuilder->getNbm($product);
        $result["origin"] = $this->productBuilder->getOrigin($product);
        $result["status"] = $this->productBuilder->getStatus($product);
        $result["type"] = $this->productBuilder->getType($product);
        $result["variantAttributes"] = $this->productBuilder->getVariantAttributes($product);
        $result["variations"] = $this->productBuilder->getVariations($product);
        return $result;
    }
}
