<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Request\BuilderProduct;

use Gubee\Integration\Engine\Gateway\Request\BuilderInterface;
use Gubee\Integration\Gateway\Config;
use Gubee\Integration\Gateway\ProductBuilder;

class Delete implements BuilderInterface
{
    protected $config;

    protected $productBuilder;

    public function __construct(
        Config $config,
        ProductBuilder $productBuilder
    ) {
        $this->config = $config;
        $this->productBuilder = $productBuilder;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        $product = $buildSubject['product'];
        $result = [
              "param" => $this->productBuilder->getId($product),
        ];
        return $result;
    }
}
