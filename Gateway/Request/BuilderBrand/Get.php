<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Request\BuilderBrand;

use Gubee\Integration\Engine\Gateway\Request\BuilderInterface;
use Gubee\Integration\Gateway\Config;
use Gubee\Integration\Gateway\BrandBuilder;

class Get implements BuilderInterface
{
    protected $config;

    protected $brandBuilder;

    public function __construct(
        Config $config,
        BrandBuilder $brandBuilder
    ) {
        $this->config = $config;
        $this->brandBuilder = $brandBuilder;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {

        $option = $buildSubject['brand'];
        $result = [
              "param" => $this->brandBuilder->getName($option),
        ];
        return $result;
    }
}
