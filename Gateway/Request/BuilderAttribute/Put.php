<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Request\BuilderAttribute;

use Gubee\Integration\Engine\Gateway\Request\BuilderInterface;
use Gubee\Integration\Gateway\Config;
use Gubee\Integration\Gateway\AttributeBuilder;

class Put implements BuilderInterface
{
    protected $config;

    protected $attributeBuilder;

    public function __construct(
        Config $config,
        AttributeBuilder $attributeBuilder
    ) {
        $this->config = $config;
        $this->attributeBuilder = $attributeBuilder;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        $attribute = $buildSubject['attribute'];
        $result = [
            "attrType" => $this->attributeBuilder->getAttrType($attribute),
            "id" => $this->attributeBuilder->getId($attribute),
            "param" => $this->attributeBuilder->getId($attribute),
            "name" => $this->attributeBuilder->getName($attribute),
            "required" => $this->attributeBuilder->getRequired($attribute),
            "variant" => $this->attributeBuilder->getVariant($attribute)
        ];
        if ($result['attrType'] == 'MULTISELECT' || $result['attrType'] == 'SELECT') {
            $result['options'] = $this->attributeBuilder->getOptions($attribute);
        }
        return $result;
    }
}
