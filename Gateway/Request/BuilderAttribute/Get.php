<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Request\BuilderAttribute;

use Gubee\Integration\Engine\Gateway\Request\BuilderInterface;
use Gubee\Integration\Gateway\Config;
use Gubee\Integration\Gateway\AttributeBuilder;

class Get implements BuilderInterface
{
    protected $config;

    protected $attributeBuilder;

    public function __construct(
        Config $config,
        AttributeBuilder $attributeBuilder
    ) {
        $this->config = $config;
        $this->attributeBuilder = $attributeBuilder;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        $attribute = $buildSubject['attribute'];
        $result = [
              "param" => $this->attributeBuilder->getId($attribute),
        ];
        return $result;
    }
}
