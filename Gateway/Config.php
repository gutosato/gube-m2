<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway;

use Gubee\Integration\Engine\Gateway\Config\Config as ConfigBase;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ReinitableConfigInterface;

class Config extends ConfigBase
{
    protected const KEY_ACTIVE = 'active';

    protected const KEY_BASE_URL = 'base_url';

    protected const KEY_TOKEN = 'token';

    protected const KEY_RENEWED_TOKEN = 'renewed_token';

    protected const KEY_BRAND_ATTRIBUTE = 'brand';

    protected const KEY_NBM_ATTRIBUTE = 'nbm';

    protected const KEY_DEPTH_ATTRIBUTE = 'depth';
    protected const KEY_HEIGHT_ATTRIBUTE = 'height';
    protected const KEY_WIDTH_ATTRIBUTE = 'width';
    protected const KEY_WEIGHT_ATTRIBUTE = 'weight';
    protected const KEY_EAN_ATTRIBUTE = 'ean';
    protected const KEY_UNIT_TIME_TYPE = 'unit_time_type';
    protected const KEY_HANDLING_TIME = 'handling_time';
    protected const KEY_WARRANTY_TIME = "default_warranty";

    protected $storeManager;

    /**
     * @param StoreManagerInterface $storeManager
     * @param WriterInterface $configWriter
     * @param ScopeConfigInterface $scopeConfig
     * @param string $methodCode
     * @param string $pathPattern
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        WriterInterface $configWriter,
        ReinitableConfigInterface $reinitableConfig,
        ScopeConfigInterface $scopeConfig,
        string $methodCode = self::KEY_METHOD_CODE,
        string $pathPattern = self::DEFAULT_PATH_PATTERN
    ) {
        parent::__construct(
            $configWriter,
            $scopeConfig,
            $reinitableConfig,
            $methodCode,
            $pathPattern
        );
        $this->storeManager = $storeManager;
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isActive(int $storeId = null): bool
    {
        return (bool)$this->getValue(self::KEY_ACTIVE, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function getToken(int $storeId = null): string
    {
        return $this->getValue(self::KEY_TOKEN, $storeId);
    }

    /**
     * @param string $renewedToken
     * @param int|null $storeId
     * @return mixed|void|null
     * @throws NoSuchEntityException
     */
    public function setRenewedToken(string $renewedToken, int $storeId = null)
    {
        if ($storeId == null) {
            $storeId = $this->getStoreId();
        }
        return $this->setValue(self::KEY_RENEWED_TOKEN, $renewedToken, $storeId);
    }

    public function getRenewedToken(int $storeId = null)
    {
        return $this->getValue(self::KEY_RENEWED_TOKEN, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function getBaseUrl(int $storeId = null): string
    {
        return $this->getValue(self::KEY_BASE_URL);
    }

    /**
     * @param int|null $storeId
     * @return ?string
     */
    public function getBrandAttribute(int $storeId = null): ?string
    {
        return $this->getValue(self::KEY_BRAND_ATTRIBUTE);
    }

    public function getDefaultBrand(int $storeId = null): string
    {
        return "214";
    }

    public function getNbm(int $storeId = null): ?string
    {
        return $this->getValue(self::KEY_NBM_ATTRIBUTE);
    }

    public function getDepth()
    {
        return $this->getValue(self::KEY_DEPTH_ATTRIBUTE);
    }

    public function getHeight()
    {
        return $this->getValue(self::KEY_HEIGHT_ATTRIBUTE);
    }

    public function getWidth()
    {
        return $this->getValue(self::KEY_WIDTH_ATTRIBUTE);
    }

    public function getWeight()
    {
        return $this->getValue(self::KEY_WEIGHT_ATTRIBUTE);
    }

    public function getEan()
    {
        return $this->getValue(self::KEY_EAN_ATTRIBUTE);
    }

    public function getUnitTime()
    {
        return $this->getValue(self::KEY_UNIT_TIME_TYPE);
    }

    public function getHandlingTime()
    {
        return $this->getValue(self::KEY_HANDLING_TIME);
    }

    /**
     * @return int
     * @throws NoSuchEntityException
     */
    public function getStoreId(): int
    {
        return (int)$this->storeManager->getStore()->getId();
    }

    public function getWarrantyTime()
    {
        return $this->getValue(self::KEY_WARRANTY_TIME);
    }
}
