<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway;

use Magento\Catalog\Model\Category;

class CategoryBuilder
{
    public function getActive( Category $category): bool
    {
        $result = false;
        if ($category->getIsActive()){
            $result = true;
        }
        return $result;
    }

    public function getDescription(Category $category): ?string
    {

        return $category->getDescription();
    }

    public function getId(Category $category): string
    {
        return (string)$category->getId();
    }

    public function getName(Category $category): string
    {
        return $category->getName();
    }

    public function getParent(Category $category): string
    {
        return (string)$category->getParentId();
    }

}