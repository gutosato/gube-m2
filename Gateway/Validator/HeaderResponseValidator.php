<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Validator;

use Gubee\Integration\Engine\Gateway\Validator\AbstractValidator;
use Gubee\Integration\Engine\Gateway\Validator\resultInterface;

class HeaderResponseValidator extends AbstractValidator
{

    /**
     * @inheritDoc
     */
    public function validate($validationSubject): resultInterface
    {
        $isValid = true;
        $fails = [];
        $errorCode = [];

        if (isset($validationSubject['response']['statusCode'])) {
            $isValid = false;

            $errorCode[] = $validationSubject['response']['statusCode'];
            $fails[] = $validationSubject['response']['message'];
        }

        return $this->createResult($isValid, $fails, $errorCode);
    }
}
