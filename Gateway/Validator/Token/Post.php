<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Validator\Token;

use Gubee\Integration\Engine\Gateway\Validator\resultInterface;
use Gubee\Integration\Engine\Gateway\Validator\AbstractValidator;
use PHP_CodeSniffer\Standards\Generic\Sniffs\Commenting\TodoSniff;

class Post extends AbstractValidator
{

    /**
     * @inheritDoc
     */
    public function validate($validationSubject): resultInterface
    {
        /* TODO: Validação token */
        $response = $validationSubject['response'];
        $isValid = true;
        $fails = [];
        $errorCodes = [];
//        $code = $response->getStatus();
//        if ($code != 200){
//            $isValid = false;
//            $errorCodes[] = $code;
//            $fails[] = $response->getMessage();
//        }
        return $this->createResult($isValid, $fails, $errorCodes);
    }
}
