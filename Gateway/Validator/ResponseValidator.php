<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Validator;


use Gubee\Integration\Engine\Gateway\Validator\AbstractValidator;
use Gubee\Integration\Engine\Gateway\Validator\ResultInterface;
use Gubee\Integration\Engine\Gateway\Validator\ResultInterfaceFactory;
use Gubee\Integration\Command\Publisher\Republisher;
use Gubee\Integration\Command\RenewToken;


class ResponseValidator extends AbstractValidator
{
    protected $commandRepublisher;
    protected $commandRenewToken;


    public function __construct(
        ResultInterfaceFactory $resultFactory,
        Republisher $commandRepublisher,
        RenewToken $commandRenewToken
    ) {
        $this->commandRepublisher = $commandRepublisher;
        $this->commandRenewToken = $commandRenewToken;
        parent::__construct($resultFactory);
    }

    /**
     * @inheritDoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validate($validationSubject): ResultInterface
    {
        $isValid = true;
        $fails = [];
        $errorCodes = [];
        $response = $validationSubject['response'];
        if (is_array($response)){
            $isValid = false;
            $this->commandRepublisher->execute($validationSubject);
        }
        if ($response->getStatus() == 401) {
            $isValid = false;
            $errorCodes[] = $response->getStatus();
            $fails[] = $response->getMessage();
            $this->commandRenewToken->execute();
            $this->commandRepublisher->execute($validationSubject);
        }
        return $this->createResult($isValid, $fails, $errorCodes);
    }
}
