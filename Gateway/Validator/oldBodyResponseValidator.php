<?php

declare(strict_types=1);

namespace Gubee\Integration\Gateway\Validator;

use Gubee\Integration\Engine\Gateway\Validator\AbstractValidator;
use Gubee\Integration\Engine\Gateway\Validator\resultInterface;

class oldBodyResponseValidator extends AbstractValidator
{

    /**
     * @inheritDoc
     */
    public function validate($validationSubject): resultInterface
    {
        $isValid = true;
        $fails = [];
        $errorCodes = [];

        if (isset($validationSubject['response']['error'])) {
            $isValid = false;

            $errorCodes = $validationSubject['response']['error']['code'];
            $fails[] = $validationSubject['response']['error']['message'];
        }

        return $this->createResult($isValid, $fails, $errorCodes);
    }
}
